# CI/CD Test app for signed commits

## build

- Create a project: `oc new project cicd-signed-commit-tests`
- make build: `kustomize build builder/ | oc apply -f -`

## deploy

- Test deployment with: `kustomize build builder/ | oc apply -f -`

## further steps are done in ArgoCD

- Setup in ArgoCD with the supplied objects in directory `./argocd`
- copy app from internal repo to external repo
- update digest in `kustomize/kustomization.yaml`
- deploy with ArgoCD
 