---
title: "PoC: Signatur von Kubernetes Containern: Digest Verification"
subject: "PoC: Signatur von Kubernetes Containern: Digest Verification"
author:
  - "Peter Pfläging <peter@pflaeging.net>"
date: 2021-09-14 v1
lang: de
toc: false
---
# Alternative Methode: Digest Verification

Nachdem die Methode einer direkten Image Signatur noch nicht produktsfähig ist, muss eine andere sichere, nachvollziehbare Methode etabliert werden.

## Vorgangsweise (Theorie)

1. Es wird ein Image generiert und in einem Development / Test Cluster getestet und für den Produktionsbetrieb freigegegeben
1. In einem Manifest Repo für das Production Delivery wird im Manifest nicht auf Tags, sondern auf das freigegebene Digest des Images referenziert.
1. Der Commit für das Delivery wird mit einem dafür speziell erzeugtem GPG Schlüssel der Commit signiert und anschließend auf den Server gepusht.
1. Der Delivery Prozess im Delivery Tool ist so konfiguriert, das nur ein signierter Commit deployed wird.
1. Das Deilvery des signierten Commits kann nun entweder automatisch oder benutzergesteuert ausgeführt werden.

## Testumfeld und Ablauf

Es wurde ein Test als Beispielimplementierung durchgeführt. 

### Testaufbau

- OKD / Openshift 4 & Kubernetes 1.22 (ARM64)
- ArgoCD
- kustomize (Teil von ArgoCD)
- git / gitlab (beliebiger Git Repo Server)
- Harbor Container Registry

### Ablauf

1. Build einer TestApp (pingpong-app) aus dem Repo <https://gitlab.pflaeging.net/brz/CICD-signed-commit-tests>
1. Kopieren des Images in die externe Registry:

         docker pull \
          default-route-openshift-image-registry.apps.mirkwood.pfpk.pro/cicd-signed-commit-tests/pingpong-app:latest
         docker tag  \
          default-route-openshift-image-registry.apps.mirkwood.pfpk.pro/cicd-signed-commit-tests/pingpong-app:latest \
          reg.pflaeging.net/sig-poc/pingpong-app:latest
         docker push reg.pflaeging.net/sig-poc/pingpong-app:latest

1. Generieren eines GPG Keys für die Signatur des Deployments:

         gpg --generate-key
         gpg -a export <Generierte Key ID> > mykey.asc
         gpg --list-key
         export SIGNKEY=$(gpg --list-key mirkwood@pfpk.pro | grep '^\s' | sed "s/ //g")

1. Setup in ArgoCD:

         argocd gpg add --from mykey.asc

1. Diesen Key zu der "Project" Definition in ArgoCD hinzufügen. Es können damit nur Applikationen von diesem Projekt deployed werden, die mit einem der GPG Schlüssel signiert sind.
1. Die Applikation wie im Repo angegeben erzeugen.
1. Es ist nun nur möglich, signierte Commits zu deployen.
1. Den Digest des oben genannten Pushes kann man entweder im Web GUI der Registry finden oder mit dem folgenden kommando finden:

         docker image inspect reg.pflaeging.net/sig-poc/pingpong-app:latest \
           | grep reg.pflaeging.net/sig-poc/pingpong-app@

1. den oben gefundenen Digest im Repo in `./kustomize/kustomization.yaml` eintragen (gilt für das Beispiel)
1. Beispiel für ein signiertes Commit:

         git commit --allow-empty -m "Freigabe Deployment 20210914" -a -S$SIGNKEY

1. Anschließend kann in ArgoCD die Applikation deployed werden. Für nicht signierte Commits wird ein Error ausgegeben.

## Überlegungen für einen Produktionsbetrieb

Zwar ist es auf diese Art und Weise relativ einfach, signierte Commits für ein Delivery zu erzeugen.

In Anbetracht der Tatsache, das immer mehr Konfigurationen nicht in den Containern sondern in den Manifesten liegt, sollte auf jeden Fall im Produktionsbetrieb mit signierten Commits gearbeitet werden. Idealerweise wird dies mit Leer-Commits wie oben beschrieben erzeugt.

Am ehesten bietet sich hier also ein automatisierter, abgesicherter Workflow an, der den Freigebenden unterstützt. Glücklicherweise sind im Idealfall nur 2 GIT Kommandos (git commit -S & git push) notwendig, um eine Freigabe zu ermögloichen. Die organisatorischen Regeln zur Überprüfung der Konformität sind hier weitaus wichtiger:

- wurde das Image getestet (Security, Funktionalität, Datenschutz)?
- Sind alle Manifeste korrekt?
- Ist das in der Registry gepushte Image ident zu dem generierten Testimage?

Weiterhin muss es eine externe Container Registry geben, die Folgendes garantiert:

- Aufbewahrung der Images nach einem vorgegebenen Mechanismus
- Stabile SHA256 Digest Hashes für jedes Image
- Saubere Protokollierung

