FROM registry.access.redhat.com/ubi8:latest

# set some metadata
LABEL summary="PingPong-App" \
      maintainer="Peter Pflaeging <peter@pflaeging.net>" \
      io.openshift.expose-services="5000:http"

USER 0
# Install base package
RUN dnf update -y && dnf clean all
RUN dnf install -y python3 python3-jinja2 openssh-clients net-tools iputils socat && dnf clean all

# make /etc/passwd for group 0 writable
RUN chmod g=u /etc/passwd

# setup APP dir
ENV HOME=/opt/app-root/pingpong/

RUN mkdir -p $HOME/export && chmod -R g=u $HOME
WORKDIR $HOME

# add flask in $HOME/.local
# ADD flask.tar.gz $HOME
RUN pip-3 install flask kubernetes

# copy entrypoint
ADD docker-entrypoint.sh $HOME
RUN chmod 755 $HOME/docker-entrypoint.sh

# copy "APP" and set environment
ADD pingpong.py $HOME
ENV FLASK_APP=pingpong.py
ENV FLASK_ENV=production

# setups
VOLUME $HOME/export
EXPOSE 5000

USER 11177:0
# entrypoint makes setups
ENTRYPOINT ["/opt/app-root/pingpong/docker-entrypoint.sh"]
# cmd sets default command
CMD ["/usr/local/bin/flask", "run", "--host=0.0.0.0"]
