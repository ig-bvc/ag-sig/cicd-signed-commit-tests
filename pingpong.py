
from flask import Flask
from datetime import datetime
import os

app = Flask(__name__)

@app.route("/")
def pingpong():
    now = datetime.now().strftime("%c")
    # make HTML
    html = "<html><title>PingPong App</title><body>"
    html += "<h2>PingPong App at " + now + " on " + os.environ["HOSTNAME"] + "</H2>"
    html += "<h3>Environment:</h3>"
    html += '<table stye="width:100%">'
    html += '<tr><th>Variable</th><th>Value</th></tr>'
    for param in os.environ.keys():
        html += "\n" + "<tr><td>%s</td><td>%s</td></tr>" % (param,os.environ[param])
    html += "</table>"
    #
    # print date
    html += "<hr/>"
    html += now
    # end HTML
    html += "</body></html>"        
    return html
